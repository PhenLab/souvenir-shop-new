package com.example.themaninthemiddl.souvenirshop.activities;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.themaninthemiddl.souvenirshop.MainActivity;
import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private EditText edtCurrentPassword, edtNewPasswrod, edtConfirmPassword;
    private AppCompatButton btnChangePassword;



    private Authentication authentication;
    //server
    Map<String,String> param = new HashMap<String, String>();

    //server
    RequestResult resultCallback = null;
    VolleyService volleyService;
    private int checkMatchPasswordCallbackCode = 1;
    private int updateUserPasswordCallbackCode = 2;

    private String checkMatchPasswordUrl = "/user/check-if-pass-match";
    private String updateUserPasswordUserUrl = "/update-user-password";
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        toolbar= (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));

        getSupportActionBar().setTitle(getResources().getString(R.string.chang_password));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();

            }
        });

        edtCurrentPassword = (EditText)findViewById(R.id.edt_current_pass);
        edtNewPasswrod = (EditText)findViewById(R.id.edt_new_password);
        edtConfirmPassword = (EditText)findViewById(R.id.edt_conf_new_password);
        btnChangePassword = (AppCompatButton) findViewById(R.id.btn_save_new_password);

        btnChangePassword.setOnClickListener(this);
        initVolleyCallback();
        authentication = new Authentication(this);
        volleyService = new VolleyService(resultCallback,this);

        swipeRefreshLayout.setEnabled(false);

    }

    private void initVolleyCallback() {

        resultCallback = new RequestResult() {
            @Override
            public void notifySuccess(int callBackCode, JSONObject response) {


                if (callBackCode == checkMatchPasswordCallbackCode) {
                    Log.i("check_if_match", response+ "");

                    try {
                        String is_match = response.getString("data");
                        Log.i("check_if_match1", response.getString("data") + "");

                        if (is_match.equals("true")) {
                            if (edtNewPasswrod.getText().toString().equals(edtConfirmPassword.getText().toString())) {
                                param.put("password", edtNewPasswrod.getText().toString());
                                volleyService.makeApiRequest(updateUserPasswordCallbackCode, updateUserPasswordUserUrl, param);
                            } else {
                                Toast.makeText(ChangePasswordActivity.this, getString(R.string.error_confirm_pass_change), Toast.LENGTH_SHORT).show();
                            }
                        }
                        swipeRefreshLayout.setRefreshing(false);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else {

                    Toast.makeText(ChangePasswordActivity.this, getString(R.string.confirm_pass_change_success), Toast.LENGTH_SHORT).show();
                    onBackPressed();
                    finish();
                }

            }

            @Override
            public void notifyError(int callBackCode, VolleyError error) {
                Log.i("request_error",error.getMessage()+"");

                swipeRefreshLayout.setRefreshing(false);
                swipeRefreshLayout.setEnabled(false);
            }
        };
    }

    @Override
    public void onClick(View view) {

        swipeRefreshLayout.setRefreshing(true);
        param.put("user_id",authentication.getUserPreference().getString("id",null));
        param.put("password",edtCurrentPassword.getText().toString());
        Log.i("check_change",param+"");
        volleyService.makeApiRequest(checkMatchPasswordCallbackCode,checkMatchPasswordUrl,param);

    }
}
