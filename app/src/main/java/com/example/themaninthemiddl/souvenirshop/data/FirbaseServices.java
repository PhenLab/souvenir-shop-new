package com.example.themaninthemiddl.souvenirshop.data;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

/**
 * Created by The Man In The Middl on 5/28/2018.
 */

public class FirbaseServices {

    static DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("chats");


    public static Query getShopChatList(int shopId ){

        Query query = mDatabase.child("chat_list").orderByChild("shop_id").equalTo(shopId);

        return query;

    }
    public static Query getCustomerChatList(int customerId ){

        Query query = mDatabase.child("chat_list").orderByChild("customer_id").equalTo(customerId);

        return query;

    }

    public static DatabaseReference getChatReference( ){

        mDatabase = mDatabase.child("message_list");

        return mDatabase;

    }

}
