package com.example.themaninthemiddl.souvenirshop.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.activities.SignInActivity;
import com.example.themaninthemiddl.souvenirshop.adapters.ChatListAdapter;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.AllChatList;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;
import com.example.themaninthemiddl.souvenirshop.data.ChatListFirebase;
import com.example.themaninthemiddl.souvenirshop.data.FirbaseServices;
import com.example.themaninthemiddl.souvenirshop.data.TimeHelper;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class MessageFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    private ProgressBar progressBar;

    List<AllChatList> allChatLists;
    ChatListAdapter chatListAdapter;
    List<ChatListFirebase> chatListFirebaseList;
    String userId = "0";

    private Authentication authentication;
    //server
    Map<String,String> param = new HashMap<String, String>();

    //server
    RequestResult resultCallback = null;
    VolleyService volleyService;
    private int getShopByIDCallbackCode = 1;
    private String getShopByIDUrl = "/get-shop-by-id";
    int storChatlistSize =0;
    ChatListFirebase chatListFirebase;
    int changeDatabaseTime= 0;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //check if login
        authentication = new Authentication(getActivity());


        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_message, container, false);

        //progress bar


        swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(this);

        //my shop recycler
        allChatLists = new ArrayList<>();


        chatListAdapter = new ChatListAdapter(getActivity(), allChatLists);
        RecyclerView recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view_message_list);


        RecyclerView.LayoutManager chatListLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(chatListLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(chatListAdapter);

        chatListFirebaseList = new ArrayList<>();
        initVolleyService();
        volleyService = new VolleyService(resultCallback, getActivity());

        if (authentication.checkIfLogedIn()) {
            Log.i("check_login_in_message","true");
            userId = authentication.getUserPreference().getString("id", "0");
            getChatListData(Integer.parseInt(userId));
        }
        return view;
    }
    @Override
    public void onResume() {

        super.onResume();
        if (!authentication.checkIfLogedIn()){
            Toast.makeText(getActivity(), getResources().getString(R.string.please_login_to_use), Toast.LENGTH_SHORT).show();
        }

    }
    private void initVolleyService() {

        resultCallback = new RequestResult() {
            @Override
            public void notifySuccess(int callBackCode, JSONObject response) {
                JSONObject jsonObject;


                try {
                    jsonObject = response.getJSONObject("data");

                    AllChatList allChatList = new AllChatList(false, chatListFirebaseList.get(storChatlistSize).isIs_shop_message(),
                            chatListFirebaseList.get(storChatlistSize).getLast_messages(),
                            jsonObject.getString("name"),
                            TimeHelper.converMiliSecondToDate(chatListFirebaseList.get(storChatlistSize).getUpdated_at()),
                            jsonObject.getString("id"),jsonObject.getString("image"),userId);
                    allChatLists.add(allChatList);
                    storChatlistSize--;

                    if (storChatlistSize >=0 ){

                        param.put("shop_id",String.valueOf(chatListFirebaseList.get(storChatlistSize).getShop_id()));
                        Log.i("check_request",""+storChatlistSize+"post"+param);

                        volleyService.makeApiRequest(getShopByIDCallbackCode,getShopByIDUrl,param);

                    }else {
                        Log.i("check_request_stop",""+allChatLists.size());

                        swipeRefreshLayout.setRefreshing(false);
                        chatListAdapter.notifyDataSetChanged();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    swipeRefreshLayout.setRefreshing(false);
                }

            }

            @Override
            public void notifyError(int callBackCode, VolleyError error) {

//                Log.i("request_error",error.getMessage());
            }
        };
    }

    public void getChatListData(int userId){


//        FirbaseServices.getCustomerChatList(userId).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                swipeRefreshLayout.setRefreshing(true);
//                allChatLists.clear();
//                chatListFirebaseList.clear();
//                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
//                    Log.i("checkValueInMessagFre", postSnapshot + "");
//                    chatListFirebase = postSnapshot.getValue(ChatListFirebase.class);
//                    chatListFirebaseList.add(chatListFirebase);
//
//                }
//                if (chatListFirebaseList.size() > 0) {
//                    storChatlistSize = chatListFirebaseList.size() - 1;
//                    param.put("shop_id", String.valueOf(chatListFirebaseList.get(storChatlistSize).getShop_id()));
//                    Log.i("check_request_out", chatListFirebaseList.size() + "," + storChatlistSize);
//                    volleyService.makeApiRequest(getShopByIDCallbackCode, getShopByIDUrl, param);
//
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });

        swipeRefreshLayout.setRefreshing(true);
        FirbaseServices.getCustomerChatList(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                allChatLists.clear();
                chatListFirebaseList.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Log.i("checkValueInMessagFre", postSnapshot + "");
                    chatListFirebase = postSnapshot.getValue(ChatListFirebase.class);
                    chatListFirebaseList.add(chatListFirebase);

                }
                if (chatListFirebaseList.size() > 0) {
                    storChatlistSize = chatListFirebaseList.size() - 1;
                    param.put("shop_id", String.valueOf(chatListFirebaseList.get(storChatlistSize).getShop_id()));
                    Log.i("check_request_out", chatListFirebaseList.size() + "," + storChatlistSize);
                    volleyService.makeApiRequest(getShopByIDCallbackCode, getShopByIDUrl, param);

                }
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        Log.i("check_time","test");
        chatListAdapter.notifyDataSetChanged();

    }


    @Override
    public void onRefresh() {

        getChatListData(Integer.parseInt(userId));
    }
}
