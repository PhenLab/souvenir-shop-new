package com.example.themaninthemiddl.souvenirshop.data;

/**
 * Created by The Man In The Middl on 5/26/2018.
 */

public class Messages {

    private String message;
    private int my_id;
    private int my_type;
    private int partner_id;
    private int partner_type;
    private String customerId_shopId;
    private Long updated_at;

    public Messages(){

    }

    public Messages(Long updated_at, String message, int my_id, int my_type, int partner_id, int partner_type, String customerId_shopId) {
        this.message = message;
        this.my_id = my_id;
        this.updated_at= updated_at;
        this.my_type = my_type;
        this.partner_id = partner_id;
        this.partner_type = partner_type;
        this.customerId_shopId = customerId_shopId;

    }

    public Long getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Long updated_at) {
        this.updated_at = updated_at;
    }

    public String getCustomerId_shopId() {
        return customerId_shopId;
    }

    public void setCustomerId_shopId(String customerId_shopId) {
        this.customerId_shopId = customerId_shopId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getMy_id() {
        return my_id;
    }

    public void setMy_id(int my_id) {
        this.my_id = my_id;
    }

    public int getMy_type() {
        return my_type;
    }

    public void setMy_type(int my_type) {
        this.my_type = my_type;
    }

    public int getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(int partner_id) {
        this.partner_id = partner_id;
    }

    public int getPartner_type() {
        return partner_type;
    }

    public void setPartner_type(int partner_type) {
        this.partner_type = partner_type;
    }
}
