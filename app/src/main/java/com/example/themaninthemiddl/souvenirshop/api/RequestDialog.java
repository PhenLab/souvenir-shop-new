package com.example.themaninthemiddl.souvenirshop.api;

import com.android.volley.VolleyError;

import org.json.JSONObject;

/**
 * Created by The Man In The Middl on 5/18/2018.
 */

public interface RequestDialog {

    public void showDialog(int dialogCallbackCode, int item);
    public void clickOnCameraOption(int dialogCallbackCode);
    public void clickOnGalleryOption(int dialogCallbackCode);


}
