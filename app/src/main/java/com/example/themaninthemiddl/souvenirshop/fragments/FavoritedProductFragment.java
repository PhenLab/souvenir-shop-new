package com.example.themaninthemiddl.souvenirshop.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.adapters.favoriteProductAdapter;
import com.example.themaninthemiddl.souvenirshop.adapters.productAdapter;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;
import com.example.themaninthemiddl.souvenirshop.data.ImageConverter;
import com.example.themaninthemiddl.souvenirshop.data.Products;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritedProductFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    private RecyclerView recyclerView;
    private favoriteProductAdapter favoritedProductAdapter;
    private List<Products> favoritedProductList;

    private ImageView imgSearchFilter;

    //data in share preference
    private Authentication authentication;
    //data in share preference


    //server
    Map<String,String> param = new HashMap<String, String>();

    //server
    RequestResult resultCallback = null;
    VolleyService volleyService;
    private int getFavoritedProductListCallbackCode = 1;
    private String getFavoritedProductUrl = "/product/favorite/by-user-id";

    private ImageView addProduct, searchFilter;
    private ImageConverter imageConverter;
    private RelativeLayout layoutNoData;
    private SwipeRefreshLayout swipeRefreshLayout;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //check if login
        authentication = new Authentication(getActivity());

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorited_product, container, false);

        layoutNoData = (RelativeLayout) view.findViewById(R.id.layout_no_data);


        swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(this);


        recyclerView = (RecyclerView)view.findViewById(R.id.favorited_product_recycler_view);

        favoritedProductList = new ArrayList<>();

        favoritedProductAdapter = new favoriteProductAdapter(getActivity(), favoritedProductList,true);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(favoritedProductAdapter);

        param.put("user_id",authentication.getUserPreference().getString("id",null));
        param.put("type",getString(R.string.product_type));

        initVolleyCallback();
        imageConverter = new ImageConverter(getActivity());
        volleyService = new VolleyService(resultCallback,getActivity());
        swipeRefreshLayout.setRefreshing(true);
        volleyService.makeApiRequest(getFavoritedProductListCallbackCode, getFavoritedProductUrl,param);


        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        //check if login
        if (!authentication.checkIfLogedIn()){
            Toast.makeText(getActivity(), getResources().getString(R.string.please_login_to_use), Toast.LENGTH_SHORT).show();
        }
    }

    private void initVolleyCallback() {
        resultCallback = new RequestResult() {
            @Override
            public void notifySuccess(int callBackCode, JSONObject response) {

                JSONArray jsonArray = null;
                JSONObject jsonObject;
                if (callBackCode == getFavoritedProductListCallbackCode) {

                    favoritedProductList.clear();

                    boolean isError = false;
                    try {
                        jsonArray = response.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            jsonObject = jsonArray.getJSONObject(i);
                            Products product = new Products(false,jsonObject.getString("id"),jsonObject.getString("image"),
                                    jsonObject.getString("name"), jsonObject.getString("price"),
                                    jsonObject.getString("category_name"), jsonObject.getString("shop_name"));
                            favoritedProductList.add(product);
                        }
                        Log.i("product_list",""+ favoritedProductList);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("rupp", "Error converting documents");
                        isError = true;
                    }
                    if (favoritedProductList.size() > 0){
                        recyclerView.setVisibility(View.VISIBLE);
                        layoutNoData.setVisibility(View.GONE);

                    }
                    else {

                        recyclerView.setVisibility(View.GONE);
                        layoutNoData.setVisibility(View.VISIBLE);
                    }
                    swipeRefreshLayout.setRefreshing(false);
                    favoritedProductAdapter.notifyDataSetChanged();
                }

                Log.i("favorite_product_data",""+response);
            }

            @Override
            public void notifyError(int callBackCode, VolleyError error) {
                Log.i("favorite_pro_data_error",""+error.getMessage());
                swipeRefreshLayout.setRefreshing(false);

            }
        };
    }


    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        volleyService.makeApiRequest(getFavoritedProductListCallbackCode, getFavoritedProductUrl,param);

    }
}
