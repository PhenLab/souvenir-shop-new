package com.example.themaninthemiddl.souvenirshop.activities;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.adapters.productAdapter;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;
import com.example.themaninthemiddl.souvenirshop.data.Products;
import com.example.themaninthemiddl.souvenirshop.data.StoreTemData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ShopProductActivity extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    String shop_id = null;
    String shop_name = null;

    RecyclerView recyclerView;
    List<Products> productList;
    productAdapter productAdapter;

    //data in share preference
    private Authentication authentication;
    //data in share preference

    //server
    Map<String,String> param = new HashMap<String, String>();

    //server
    RequestResult resultCallback = null;
    VolleyService volleyService;
    private int getProductListCallbackCode = 1;
    private String getProductListUrl = "/get-product-list-by-shop-id";

    private ImageView addProduct, searchFilter;
    private TextView toolbarTitle;
    private boolean is_owner = false;
    private boolean is_search = false;
    private StoreTemData searchMyProductCon;
    private RelativeLayout layoutNoData;

    SwipeRefreshLayout swipeRefreshLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_product);

        toolbar= (Toolbar) findViewById(R.id.toolbar);
        toolbarTitle= (TextView) findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        layoutNoData = (RelativeLayout)findViewById(R.id.layout_no_data);

        shop_id = getIntent().getStringExtra("shop_id");

        shop_name = getIntent().getStringExtra("shop_name");
        authentication  = new Authentication(this);
        searchMyProductCon = new StoreTemData(this, getString(R.string.search_my_product_condition));

        toolbarTitle.setText(shop_name+"'s "+getResources().getString(R.string.products));
//        Toast.makeText(this, "this is shop id"+ shop_id, Toast.LENGTH_SHORT).show();

        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);

        productList = new ArrayList<>();

        productAdapter = new productAdapter(getApplication(), productList,true);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplication(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(productAdapter);

        initVolleyCallback();

        addProduct = (ImageView)findViewById(R.id.add_product);
        searchFilter = (ImageView)findViewById(R.id.search_filter);

        is_owner = getIntent().getBooleanExtra(getString(R.string.is_owner),false);

        if (is_owner){
            addProduct.setVisibility(View.VISIBLE);
        }

        searchFilter.setOnClickListener(this);
        addProduct.setOnClickListener(this);

        volleyService = new VolleyService(resultCallback,this);
        param.put("shop_id",shop_id);

        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));

        swipeRefreshLayout.setRefreshing(true);


        volleyService.makeApiRequest(getProductListCallbackCode,getProductListUrl,param);

    }


    @Override
    protected void onResume() {
        super.onResume();
        //check search condition
        is_search = searchMyProductCon.getPreference().getBoolean("is_search", false);

        param.put("is_search",String.valueOf(is_search));

        if (is_search){

            swipeRefreshLayout.setRefreshing(true);
            productList.clear();
            param.put("user_id", searchMyProductCon.getPreference().getString("user_id",null));
            param.put("category_id", searchMyProductCon.getPreference().getString("category_id",null));
            param.put("minPrice", searchMyProductCon.getPreference().getString("minPrice",null));
            param.put("maxPrice", searchMyProductCon.getPreference().getString("maxPrice",null));
            param.put("is_popular", String.valueOf(searchMyProductCon.getPreference().getBoolean("is_popular",false)));
            param.put("keyword", searchMyProductCon.getPreference().getString("keyword",null));

            searchMyProductCon.clearData();
            Log.i("is_search_true",is_search+""+param);
            is_search = false;
            volleyService.makeApiRequest(getProductListCallbackCode,getProductListUrl,param);

        }

    }

    private void initVolleyCallback() {

        resultCallback = new RequestResult() {
            @Override
            public void notifySuccess(int callBackCode, JSONObject response) {

                Log.i("product_list",""+response);

                JSONArray jsonArray = null;
                JSONObject jsonObject;
                if (callBackCode == getProductListCallbackCode) {
                    productList.clear();
                    boolean isError = false;
                    try {
                        jsonArray = response.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            jsonObject = jsonArray.getJSONObject(i);
                            Products product = new Products(true, jsonObject.getString("id"),jsonObject.getString("image"),
                                    jsonObject.getString("name"), jsonObject.getString("price"),
                                    jsonObject.getString("category_name"), jsonObject.getString("shop_name"));
                            productList.add(product);
                        }
                        Log.i("product_list",""+productList);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("rupp", "Error converting documents");
                        isError = true;
                    }
                    if (productList.size() > 0){
                        recyclerView.setVisibility(View.VISIBLE);
                        layoutNoData.setVisibility(View.GONE);

                    }
                    else {

                        recyclerView.setVisibility(View.GONE);
                        layoutNoData.setVisibility(View.VISIBLE);
                    }
                    productAdapter.notifyDataSetChanged();
                    swipeRefreshLayout.setRefreshing(false);
                    swipeRefreshLayout.setEnabled(false);
                }
            }

            @Override
            public void notifyError(int callBackCode, VolleyError error) {
                Log.i("product_list error ",""+error);
                swipeRefreshLayout.setRefreshing(false);
                swipeRefreshLayout.setEnabled(false);

            }
        };
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.add_product:

                intent = new Intent(getApplication(),CreateProductActivity.class);
                intent.putExtra("shop_id",shop_id);
                startActivity(intent);
                finish();
                break;

            case R.id.search_filter:
                intent = new Intent(getApplication(), MyProductSearchActivity.class);
                startActivity(intent);
                break;
        }
    }
}
