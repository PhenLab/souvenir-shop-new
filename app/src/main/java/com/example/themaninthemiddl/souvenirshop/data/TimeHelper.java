package com.example.themaninthemiddl.souvenirshop.data;

import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by The Man In The Middl on 5/28/2018.
 */

public class TimeHelper {

    public static String converMiliSecondToDate(Long miliSecond) {

        Long oneDayMiliSecond, oneYearMiliSecond;
        Long mili = Long.valueOf(1000);
        Long sixty = Long.valueOf(60);
        Long twelve = Long.valueOf(12);
        Long twentyFour = Long.valueOf(24);
        Long hourPerYear = Long.valueOf(8766);


        SimpleDateFormat sdf = null;

        oneDayMiliSecond = mili * sixty * sixty * twentyFour;
        oneYearMiliSecond = mili * sixty * sixty * hourPerYear;

        Date date = new Date(miliSecond);

        if (System.currentTimeMillis() - miliSecond < oneDayMiliSecond){

            sdf = new SimpleDateFormat(" h:mm a", Locale.ENGLISH);

        }
        else if (System.currentTimeMillis() - miliSecond > oneDayMiliSecond &&
                System.currentTimeMillis() - miliSecond < oneYearMiliSecond){

            sdf = new SimpleDateFormat("d/MM h:mm a", Locale.ENGLISH);
        }
        else {

            sdf = new SimpleDateFormat("d/MM/yyyy h:mm a", Locale.ENGLISH);


        }
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+7"));
        String formattedDate = sdf.format(date);

        return formattedDate;

    }
    public static String converMiliSecondToHour(long miliSecond) {

        long seconds, minutes = 0, hours;
        seconds = System.currentTimeMillis() / 1000;
        
        Date date = new Date(miliSecond);
        DateFormat formatter = new SimpleDateFormat("HH:mm a");
        formatter.setTimeZone(TimeZone.getTimeZone("GMT+7"));

        String dateFormatted = formatter.format(date);

        return dateFormatted;


    }
    public static  String getMiliSecond() {

        return String.valueOf(System.currentTimeMillis());

    }
}
