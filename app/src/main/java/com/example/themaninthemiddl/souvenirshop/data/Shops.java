package com.example.themaninthemiddl.souvenirshop.data;

/**
 * Created by The Man In The Middle on 4/29/2018.
 */

public class Shops {
    private String product_number, id, shop_img;
    private int favorite;
    private String name,profile;


    public Shops(String id, String shop_img,String product_number,  String shop_name) {
        this.product_number = product_number;
        this.profile = shop_img;
        this.id = id;
        this.name = shop_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setFavorite(int favorite) {
        this.favorite = favorite;
    }

    public String getShop_img() {
        return profile;
    }

    public void setShop_img(String shop_img) {
        this.profile = shop_img;
    }

    public String getProduct_number() {
        return product_number;
    }

    public void setProduct_number(String product_number) {
        this.product_number = product_number;
    }


    public String getShop_name() {
        return name;
    }

    public void setShop_name(String shop_name) {
        this.name = shop_name;
    }


}
