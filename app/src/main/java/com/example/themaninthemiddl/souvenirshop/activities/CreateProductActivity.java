package com.example.themaninthemiddl.souvenirshop.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.api.DialogService;
import com.example.themaninthemiddl.souvenirshop.api.RequestDialog;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;
import com.example.themaninthemiddl.souvenirshop.data.ImageConverter;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CreateProductActivity extends AppCompatActivity implements MaterialSpinner.OnItemSelectedListener, View.OnClickListener {

    Toolbar toolbar;
    private static final int GET_IMAGE_FROM_CAMERA_CODE = 1;
    private static final int GET_IMAGE_FROM_GALLERY_CODE = 2;
    private RequestDialog requestDialog;
    private DialogService dialogService;
    private int choosePhotoCallbackCode = 1;
    private int chooseGalleryCallbackCode = 2;


    //image
    private ImageConverter imageConverter;
    private String imageBase64String;
    private Base64 imageBitmap;

    //data in share preference
    private Authentication authentication;
    //data in share preference


    //server
    Map<String,String> param = new HashMap<String, String>();

    //server
    RequestResult resultCallback = null;
    VolleyService volleyService;
    private int createProductCallbackCode = 1;
    private String createProductUrl = "/product/add";
    private int getCategoryListCallbackCode = 2;
    private String getCategoryListUrl = "/category/list";

    String shop_id = null;
    private MaterialSpinner selectCategory;
    private List<String> categoryNameList = new ArrayList<String>();
    private List<String> categoryIdList = new ArrayList<String>();

    private EditText name, quantity, price,description;
    private ImageView image;
    private AppCompatButton btnSave;

    private String getImageBase64String;
    private Uri ImgUri;
    private Bitmap imgBitmap;

    private String selectedCategoryId = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_product);


        toolbar= (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(getResources().getString(R.string.create_new_product));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        shop_id = getIntent().getStringExtra("shop_id");


        categoryNameList.add(getResources().getString(R.string.choose_a_category));
        categoryIdList.add("0");

        selectCategory = (MaterialSpinner)findViewById(R.id.select_category);


        selectCategory.setOnItemSelectedListener(this);

        name = (EditText)findViewById(R.id.product_name);
        quantity = (EditText)findViewById(R.id.quantity);
        price = (EditText)findViewById(R.id.price);
        description = (EditText)findViewById(R.id.description);
        image = (ImageView) findViewById(R.id.image);
        btnSave = (AppCompatButton) findViewById(R.id.btn_save_product);
        btnSave.setOnClickListener(this);
        image.setOnClickListener(this);


        imageConverter = new ImageConverter(this);
        authentication = new Authentication(this);
//        // initialize all your visual fields
//        if (savedInstanceState != null) {
//            selectCategory.setSelectedIndex(savedInstanceState.getInt("selectCategorySpinner", 0));
//            // do this for each of your text views
//        }
        initVolleyCallback();
        volleyService = new VolleyService(resultCallback,this);
        volleyService.makeApiRequest(getCategoryListCallbackCode,getCategoryListUrl,param);

        //choose photo

        initDialogCallback();
        dialogService = new DialogService(requestDialog,this);
    }

    private void initDialogCallback() {

        requestDialog = new RequestDialog() {
            @Override
            public void showDialog(int dialogCallbackCode, int item) {

            }

            @Override
            public void clickOnCameraOption(int dialogCallbackCode) {

                if (dialogCallbackCode == choosePhotoCallbackCode)
                    launchCameraClick();

            }

            @Override
            public void clickOnGalleryOption(int dialogCallbackCode) {
                if (dialogCallbackCode == choosePhotoCallbackCode)
                    openGalleryClick();
            }
        };
    }


    private void initVolleyCallback() {

        resultCallback = new RequestResult() {
            @Override
            public void notifySuccess(int callBackCode, JSONObject response) {

                if (callBackCode == getCategoryListCallbackCode) {
                    JSONArray jsonArray = null;
                    JSONObject jsonObject;

                    try {
                        jsonArray = response.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            jsonObject = jsonArray.getJSONObject(i);
                            Log.i("get_category", "" + jsonObject);
                            categoryNameList.add(jsonObject.getString("name"));
                            categoryIdList.add(jsonObject.getString("id"));


                        }
                        selectCategory.setItems(categoryNameList);

                        Log.i("category_name", "" + categoryNameList);
                        Log.i("category_id", "" + categoryIdList);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else {

                    String product_id;
                    try {
                        product_id = response.getString("id");
                        Intent intent = new Intent(getApplication(),ProductDetailActivity.class);
                        intent.putExtra("product_id",product_id);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void notifyError(int callBackCode, VolleyError error) {

                Log.i("get_error",""+ error);
            }
        };

    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

        selectedCategoryId = categoryIdList.get(position);
//
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.image:
                dialogService.makeChoosePhotoOptionDialog(choosePhotoCallbackCode);

                break;

            case R.id.btn_save_product:
                param.clear();
                param.put("name",name.getText().toString());
                param.put("price",price.getText().toString());
                param.put("quantity",quantity.getText().toString());
                param.put("description",description.getText().toString());
                param.put("image",imageBase64String);
                param.put("shop_id",shop_id);
                param.put("user_id",authentication.getUserPreference().getString("id",null));

                param.put("category_id",selectedCategoryId);

                Log.i("post_data",""+param);
                volleyService.makeApiRequest(createProductCallbackCode,createProductUrl,param);
                break;


        }
    }
//
//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//        outState.putInt("selectCategorySpinner", selectCategory.getSelectedIndex());
//        // do this for each or your Spinner
//        // You might consider using Bundle.putStringArray() instead
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GET_IMAGE_FROM_GALLERY_CODE && resultCode == RESULT_OK){

            ImgUri = data.getData();
            imgBitmap = imageConverter.loadFitImage(ImgUri,300,300);

            image.setImageBitmap(imgBitmap);
            //convert from bitmap to base64 string
            imageBase64String = imageConverter.convertFromBitmapToBase64(imgBitmap);

            Log.i("base64",","+imageBase64String+",");

        }
        else if (requestCode == GET_IMAGE_FROM_CAMERA_CODE && resultCode == RESULT_OK){

            Bitmap bitmap = (Bitmap) data.getExtras().get("data");

            imgBitmap = imageConverter.loadFitImage(imageConverter.convertFromBitmapToUri(bitmap),500,500);
            image.setImageBitmap(imgBitmap);

            //convert from bitmap to base64 string
            imageBase64String = imageConverter.convertFromBitmapToBase64(imgBitmap);


        }
    }


    //camera
    public void launchCameraClick(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent,GET_IMAGE_FROM_CAMERA_CODE);

    }

    //gallery
    public void openGalleryClick(){

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent,GET_IMAGE_FROM_GALLERY_CODE);

    }

}
