package com.example.themaninthemiddl.souvenirshop.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.activities.ChatActivity;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.AllChatList;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;
import com.example.themaninthemiddl.souvenirshop.data.ChatMessages;
import com.example.themaninthemiddl.souvenirshop.data.ImageConverter;
import com.example.themaninthemiddl.souvenirshop.data.TimeHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Created by The Man In The Middl on 4/29/2018.
 */

public class ChatMessageAdapter extends RecyclerView.Adapter<ChatMessageAdapter.MyViewHolder> {

    private Context mContext;
    private List<ChatMessages> chatMessagesList;
    private ImageConverter imageConverter;
    private String base64String;
    boolean isCustomise = false;

    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;
    private int storeViewID = 0;
    Authentication authentication;
    int testStep = 0;

    private int getShopByIDCallbackCode = 20;
    private String getShopByIDUrl = "/get-shop-by-id";
    private VolleyService volleyService;
    //server
    Map<String, String> param = new HashMap<String, String>();
    RequestResult resultCallback = null;


    private RecyclerViewItemClickListener recyclerViewItemClickListener;
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Log.i("in_onCreateViewHolder",testStep+"");
        testStep++;
        imageConverter = new ImageConverter(mContext);
        View itemView;
        Log.i("check_view_type",""+viewType);
        if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
            storeViewID = VIEW_TYPE_MESSAGE_RECEIVED;
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_received, parent, false);
        }
        else {
            storeViewID = VIEW_TYPE_MESSAGE_SENT;
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_sent, parent, false);
        }

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Log.i("in_onBindViewHolder", testStep + "");
        testStep++;
        ChatMessages chatMessage = chatMessagesList.get(position);

        if (holder.getItemViewType() == VIEW_TYPE_MESSAGE_RECEIVED) {

            holder.tvMesage.setText(chatMessage.getMessageText());
            holder.tvMessageTime.setText(TimeHelper.converMiliSecondToDate(chatMessage.getMessageTime()));
//            holder.tvMesageSender.setText(chatMessage.getMessageSender());

            if (!chatMessage.getSenderImage().equals("null")) {
                Log.i("check_img", chatMessage.getSenderImage());
                holder.senderImage.setImageBitmap(imageConverter.convertFromBase64ToBitmap(chatMessage.getSenderImage()));
            }

        } else {
            holder.tvMesage.setText(chatMessage.getMessageText());
            holder.tvMessageTime.setText(TimeHelper.converMiliSecondToDate(chatMessage.getMessageTime()));

        }
    }

    @Override
    public int getItemCount() {
        return chatMessagesList.size();

    }



    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener{
        public TextView  tvMesage, tvMessageTime, tvMesageSender;
        public ImageView senderImage;


        public MyViewHolder(View view) {
            super(view);
            Log.i("in_MyViewHolder",testStep+""+getItemViewType());
            testStep++;
            if (storeViewID == VIEW_TYPE_MESSAGE_RECEIVED) {
                senderImage = (ImageView) view.findViewById(R.id.sender_image);
                tvMessageTime = (TextView) view.findViewById(R.id.tv_sender_message_time);
                tvMesage = (TextView) view.findViewById(R.id.tv_sender_message);
//                tvMesageSender = (TextView) view.findViewById(R.id.tv_sender_name);

                senderImage.setOnLongClickListener(this);
                tvMesage.setOnLongClickListener(this);
            }
            else {

            tvMessageTime = (TextView) view.findViewById(R.id.tv_my_message_time);
            tvMesage = (TextView) view.findViewById(R.id.tv_my_message);
            tvMesage.setOnLongClickListener(this);
            }
        }

        @Override
        public boolean onLongClick(View view) {
            return false;
        }
    }

    @Override
    public int getItemViewType(int position) {
        ChatMessages chatMessages = chatMessagesList.get(position);
        Log.i("in_getItemViewType",testStep+"");
        testStep++;
        authentication = new Authentication(mContext);
        volleyService = new VolleyService(resultCallback,mContext);

        Log.i("check_mine",chatMessages.getMyID()+""+chatMessages.getMyType());
        if (chatMessages.getMyType().equals(mContext.getString(R.string.shop_owner_type)) && chatMessages.isOwner()){
                    // If the current user is the sender of the message
            return VIEW_TYPE_MESSAGE_SENT;

        } else if (chatMessages.getMyID().equals(authentication.getUserPreference().getString("id",null)) &&
                    chatMessages.getMyType().equals(mContext.getString(R.string.customer_type))){
            // If some other user sent the message
            return VIEW_TYPE_MESSAGE_SENT;
        }
        else {
            return VIEW_TYPE_MESSAGE_RECEIVED;
        }
    }

    public ChatMessageAdapter(Context mContext, List<ChatMessages> chatMessagesList) {

        this.mContext = mContext;
        this.chatMessagesList = chatMessagesList;

    }

}
