package com.example.themaninthemiddl.souvenirshop.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.activities.ProductDetailActivity;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;
import com.example.themaninthemiddl.souvenirshop.data.ImageConverter;
import com.example.themaninthemiddl.souvenirshop.data.ProductGallery;
import com.example.themaninthemiddl.souvenirshop.data.Products;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by The Man In The Middl on 4/29/2018.
 */

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.MyViewHolder>  {

    private Context mContext;
    private List<ProductGallery> productGalleryList;
    private ImageConverter imageConverter;


    //data in share preference
    private String userData = "user_data";
    private Authentication authentication;
    //data in share preference


    //server
    Map<String,String> param = new HashMap<String, String>();

    //server
    RequestResult resultCallback = null;
    VolleyService volleyService;

    private String deleteFavoritedProductUrl = "/favorite/delete";
    private int deleteFavoritedProductCallbackCode = 10;

    private productAdapter productAdapter;
    private List<Products> relatedProductList = new ArrayList<>();

    CollapsingToolbarLayout toolbarLayout;
    private RecyclerView relateProductRecyclerView;
    AppCompatButton btnContact;
    String product_id = null;
    int position = -1;


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        imageConverter = new ImageConverter(mContext);
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.gallery_card, parent, false);

        authentication = new Authentication(mContext);
        volleyService = new VolleyService(resultCallback , mContext);

        return new MyViewHolder(itemView);



    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Log.i("check_size",productGalleryList.size()+"");
        ProductGallery gallery = productGalleryList.get(position);

        if (!(gallery.getImage()).equals("null")) {
            holder.imvGallery.setImageBitmap(imageConverter.convertFromBase64ToBitmap(gallery.getImage()));
        }

    }

    @Override
    public int getItemCount() {
        return productGalleryList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public ImageView imvGallery;

        public MyViewHolder(View view) {
            super(view);
            imvGallery = (ImageView) view.findViewById(R.id.imv_gallery);
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {

            position = this.getAdapterPosition();
            ProductGallery gallery = productGalleryList.get(position);


        }
    }

    public GalleryAdapter(Context mContext, List<ProductGallery> productGalleryList) {

        this.mContext = mContext;
        this.productGalleryList = productGalleryList;

    }


}
