package com.example.themaninthemiddl.souvenirshop.data;

/**
 * Created by The Man In The Middl on 5/25/2018.
 */

public class AllChatList {

    private String lastMessage;
    private String partnerName;
    private String lastMessageTime;
    private String shopId;
    private String customerId;
    private String partnerImage;
    private boolean is_shop_message;
    private boolean is_shop_owner;


    public boolean isIs_shop_owner() {
        return is_shop_owner;
    }

    public void setIs_shop_owner(boolean is_shop_owner) {
        this.is_shop_owner = is_shop_owner;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getPartnerImage() {
        return partnerImage;
    }

    public void setPartnerImage(String partnerImage) {
        this.partnerImage = partnerImage;
    }

    public String getShopId() {

        return shopId;

    }

    public boolean isIs_shop_message() {
        return is_shop_message;
    }

    public void setIs_shop_message(boolean is_shop_message) {
        this.is_shop_message = is_shop_message;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public AllChatList(boolean is_shop_owner, boolean is_shop_message, String lastMessage, String partnerName,
                       String lastMessageTime, String shopId, String partnerImage, String customerId) {
        this.lastMessage = lastMessage;
        this.partnerName = partnerName;
        this.lastMessageTime = lastMessageTime;
        this.shopId = shopId;
        this.is_shop_owner = is_shop_owner;
        this.is_shop_message = is_shop_message;
        this.partnerImage = partnerImage;
        this.customerId = customerId;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getLastMessageTime() {
        return lastMessageTime;
    }

    public void setLastMessageTime(String lastMessageTime) {
        this.lastMessageTime = lastMessageTime;
    }
}
