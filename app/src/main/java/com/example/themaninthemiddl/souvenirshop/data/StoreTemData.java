package com.example.themaninthemiddl.souvenirshop.data;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by The Man In The Middl on 5/4/2018.
 */

public class StoreTemData {

    private static Context context;
    private static SharedPreferences.Editor editor;

    private static SharedPreferences preference;

    public StoreTemData(Context context, String key) {

        this.context = context;
        preference = context.getSharedPreferences(key, MODE_PRIVATE);
        this.editor = preference.edit();

    }

    public SharedPreferences getPreference(){

        return preference;

    }
    public SharedPreferences.Editor getEditor(){

        return editor;
    }



    public void commitDataChange(){
        editor.commit();
    }




    public void clearData(){

        preference.edit().clear().commit();

    }
}
