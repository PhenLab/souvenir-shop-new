package com.example.themaninthemiddl.souvenirshop.api;

import com.android.volley.VolleyError;

import org.json.JSONObject;

/**
 * Created by lotphen on 12/29/17.
 */

public interface RequestResult {

    public void notifySuccess(int callBackCode, JSONObject response);
    public void notifyError(int callBackCode, VolleyError error);

}
