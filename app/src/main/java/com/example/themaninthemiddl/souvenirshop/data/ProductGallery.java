package com.example.themaninthemiddl.souvenirshop.data;

/**
 * Created by The Man In The Middl on 6/5/2018.
 */

public class ProductGallery {

    private String image;
    private String product_id;
    private String gallery_id;

    public ProductGallery(String image, String product_id, String gallery_id) {
        this.image = image;
        this.product_id = product_id;
        this.gallery_id = gallery_id;
    }

    public String getGallery_id() {
        return gallery_id;
    }

    public void setGallery_id(String gallery_id) {
        this.gallery_id = gallery_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }
}
