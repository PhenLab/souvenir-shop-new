package com.example.themaninthemiddl.souvenirshop.activities;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.themaninthemiddl.souvenirshop.MainActivity;
import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;
import com.example.themaninthemiddl.souvenirshop.data.LocationService;
import com.example.themaninthemiddl.souvenirshop.data.StoreTemData;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.florescu.android.rangeseekbar.RangeSeekBar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyProductSearchActivity extends AppCompatActivity implements MaterialSpinner.OnItemSelectedListener, View.OnClickListener {

    private RangeSeekBar skbPrice;
    private TextView tvSkbMin, tvSkbMax;
    private AppCompatButton btnPopular, btnSearch;

    private GoogleMap google_map;
    private static final int REQUEST_CHECK_SETTINGS = 0x1;
    private static GoogleApiClient mGoogleApiClient;
    private static final int ACCESS_FINE_LOCATION_INTENT_ID = 3;
    private LatLng currentLocation = null;
    private float minPrice = 0, maxPrice = 0;
    private String category = "0";
    boolean is_popular = false;
    private MaterialSpinner snCategory, snLocation;
    private List<String> categoryNameList = new ArrayList<String>();
    private List<String> categoryIdList = new ArrayList<String>();

    //data in share preference
    private Authentication authentication;
    //data in share preference


    //server
    Map<String,String> param = new HashMap<String, String>();

    //server
    RequestResult resultCallback = null;
    VolleyService volleyService;
    private int getMinMaxPriceCallbackCode = 1;
    private String getMinMaxPriceUrl = "/product/min-max-price";
    private int getCategoryListCallbackCode = 2;

    private String getMinMaxDistantUrl = "/shop/min-max-distance";
    private int getMinMaxDistantCallbackCode = 3;

    private String getCategoryListUrl = "/category/list";
    private String selectedCategoryId = "0";
    private LinearLayout layoutNearby;
    private String user_id;
    private LocationService locationService;
    private StoreTemData storeSearchCondition;
    private EditText keyword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_product_search);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(getResources().getString(R.string.search));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        skbPrice = (RangeSeekBar)findViewById(R.id.skb_price);

        tvSkbMin = (TextView)findViewById(R.id.tv_sb_min);
        tvSkbMax = (TextView)findViewById(R.id.tv_sb_max);
        btnPopular = (AppCompatButton)findViewById(R.id.btn_popular);
        btnSearch = (AppCompatButton)findViewById(R.id.btn_search);
        snCategory = (MaterialSpinner)findViewById(R.id.sn_category);

        keyword = (EditText)findViewById(R.id.keyword);
        snCategory.setOnItemSelectedListener(this);

        btnSearch.setOnClickListener(this);
        btnPopular.setOnClickListener(this);
        storeSearchCondition = new StoreTemData(getApplication(),getString(R.string.search_my_product_condition));



//        skbPrice.setRangeValues(0,100);

        skbPrice.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Object minValue, Object maxValue) {

                minPrice = Float.valueOf(minValue.toString());
                maxPrice = Float.valueOf(maxValue.toString());

                Log.i("track_value",minValue.toString()+","+maxValue);
            }
        });

        initVolleyCallback();
        volleyService = new VolleyService(resultCallback, getApplication());
        locationService = new LocationService(this);

        categoryNameList.add(getString(R.string.choose_a_category));
        categoryIdList.add("0");
        volleyService.makeApiRequest(getCategoryListCallbackCode, getCategoryListUrl,param);
        volleyService.makeApiRequest(getMinMaxPriceCallbackCode, getMinMaxPriceUrl,param);


        authentication = new Authentication(this);
        if (authentication.checkIfLogedIn()){
            user_id = authentication.getUserPreference().getString("id", null);
        }



    }

    private void initVolleyCallback() {

        resultCallback = new RequestResult() {
            @Override
            public void notifySuccess(int callBackCode, JSONObject response) {

                JSONArray jsonArray = null;
                JSONObject jsonObject;

                if (callBackCode == getCategoryListCallbackCode) {

                    try {
                        jsonArray = response.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            jsonObject = jsonArray.getJSONObject(i);
                            Log.i("get_category", "" + jsonObject);
                            categoryNameList.add(jsonObject.getString("name"));
                            categoryIdList.add(jsonObject.getString("id"));


                        }
                        snCategory.setItems(categoryNameList);

                        Log.i("category_name", "" + categoryNameList);
                        Log.i("category_id", "" + categoryIdList);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else  if(callBackCode == getMinMaxPriceCallbackCode){

                    try {
                        jsonArray = response.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            jsonObject = jsonArray.getJSONObject(i);

                            minPrice = Float.valueOf(jsonObject.getString("min_price"));
                            maxPrice = Float.valueOf(jsonObject.getString("max_price"));


                            skbPrice.setRangeValues(Math.round(Float.valueOf(jsonObject.getString("min_price"))),
                                    Math.round(Float.valueOf(jsonObject.getString("max_price")))+1);

                            skbPrice.setSelectedMaxValue(Math.round(Float.valueOf(jsonObject.getString("max_price")))+1);
                            tvSkbMin.setText(String.valueOf(Math.round(Float.valueOf(jsonObject.getString("min_price")))));
                            tvSkbMax.setText(String.valueOf(Math.round(Float.valueOf(jsonObject.getString("max_price")))+1));

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void notifyError(int callBackCode, VolleyError error) {

                Log.i("get_error",""+ error);
            }
        };

    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

        switch (view.getId()){
            case R.id.sn_category:
                selectedCategoryId = categoryIdList.get(position);
                Log.i("categoryId",categoryIdList.get(position));
                break;

        }
    }

    @Override
    public void onClick(View view) {

        param.put("user_id",user_id);
        param.put("category_id",selectedCategoryId);
        param.put("minPrice",String.valueOf(minPrice));
        param.put("maxPrice",String.valueOf(maxPrice));


        if (currentLocation != null) {
            param.put("current_lat", String.valueOf(currentLocation.latitude));
            param.put("current_lng", String.valueOf(currentLocation.longitude));
        }

        switch (view.getId()){

            case R.id.btn_popular:
                is_popular = true;
                Toast.makeText(this, "popular", Toast.LENGTH_SHORT).show();
                break;

            case R.id.btn_search:
                is_popular = false;
                Toast.makeText(this, "search", Toast.LENGTH_SHORT).show();

                break;
        }

        storeSearchCondition.getEditor().putString("user_id",user_id);
        storeSearchCondition.getEditor().putString("category_id",selectedCategoryId);
        storeSearchCondition.getEditor().putString("minPrice",String.valueOf(minPrice));
        storeSearchCondition.getEditor().putString("maxPrice",String.valueOf(maxPrice));
        storeSearchCondition.getEditor().putBoolean("is_popular",is_popular);
        storeSearchCondition.getEditor().putBoolean("is_search", true);
        storeSearchCondition.getEditor().putString("keyword", keyword.getText().toString());


        storeSearchCondition.commitDataChange();

        Log.i("check_search_data",storeSearchCondition.getPreference().getString("category_id", "0")+""+param);

        onBackPressed();
        finish();

    }

}
