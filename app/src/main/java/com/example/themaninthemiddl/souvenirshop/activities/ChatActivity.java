package com.example.themaninthemiddl.souvenirshop.activities;

import android.app.Activity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.adapters.ChatListAdapter;
import com.example.themaninthemiddl.souvenirshop.adapters.ChatMessageAdapter;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;
import com.example.themaninthemiddl.souvenirshop.data.ChatListFirebase;
import com.example.themaninthemiddl.souvenirshop.data.ChatMessages;
import com.example.themaninthemiddl.souvenirshop.data.ImageConverter;
import com.example.themaninthemiddl.souvenirshop.data.Messages;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TimeZone;


public class ChatActivity extends AppCompatActivity {

    Toolbar toolbar;

    ChatMessageAdapter chatMessageAdapter;
    EditText edtMessage;
    Button btnSend;
    String shop_owner_id = "0";
    boolean is_owner = false;
    String customerId_shopId;
    ImageView imvSenderImage;
    TextView tvSenderName;
    private String shop_id = null, customer_id= null;

    private Authentication authentication;
    //server
    Map<String,String> param = new HashMap<String, String>();

    //server
    RequestResult resultCallback = null;
    VolleyService volleyService;
    private int getUserByIDCallbackCode = 1;
    private int updateUserCallbackCode = 2;

    private String getUserByIDUrl = "/get-user-by-id";
    private String updateUserUrl = "/update-user-image";
    private SwipeRefreshLayout swipeRefreshLayout;
    private List<ChatMessages> chatMessagesList = new ArrayList<>();
    private int my_id=0,my_type =0, partner_id=0,partner_type = 0;
    private String senderName= null, senderImage= null;

    private Messages message;
    private  DatabaseReference databaseReference;
    private int trackEventChangeListenerTime = 0;

    private ImageConverter imageConverter;
    private DatabaseReference chatListRef;
    private RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        toolbar= (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(getResources().getString(R.string.chat));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        edtMessage = (EditText)findViewById(R.id.ed_text_chatbox);
        btnSend = (Button)findViewById(R.id.btn_send_message);
        imvSenderImage = (ImageView) findViewById(R.id.imv_sender_image);
        tvSenderName = (TextView) findViewById(R.id.tv_sender_name);
        btnSend.setEnabled(false);

        edtMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (!edtMessage.getText().equals(null) || !edtMessage.getText().equals("") || !edtMessage.getText().equals("null"))
                    btnSend.setEnabled(true);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        shop_owner_id = getIntent().getStringExtra("shop_owner_id");
        shop_owner_id = "1";
        initVolleyService();
        authentication = new Authentication(this);
        volleyService = new VolleyService(resultCallback, this);
        imageConverter = new ImageConverter(this);

        is_owner = getIntent().getBooleanExtra(getString(R.string.is_owner),false);
        if (is_owner == true){
            my_type =Integer.parseInt(getString(R.string.shop_owner_type));
            partner_type =Integer.parseInt(getString(R.string.customer_type));

            customer_id = getIntent().getStringExtra(getString(R.string.customer_id));
            shop_id = getIntent().getStringExtra(getString(R.string.shop_id));
            my_id = Integer.parseInt(shop_id);

            partner_id = Integer.parseInt(customer_id);
            senderImage = getIntent().getStringExtra(getString(R.string.customer_image));

            senderName = getIntent().getStringExtra(getString(R.string.customer_name));


            tvSenderName.setText(senderName);
            if (!senderImage.equals("null")) {
                imvSenderImage.setImageBitmap(imageConverter.convertFromBase64ToBitmap(senderImage));
            }
        }
        else {

            my_type =Integer.parseInt(getString(R.string.customer_type));
            partner_type =Integer.parseInt(getString(R.string.shop_owner_type));

            customer_id = authentication.getUserPreference().getString("id",null);

            shop_id = getIntent().getStringExtra(getString(R.string.shop_id));
            my_id = Integer.parseInt(customer_id);

            partner_id = Integer.parseInt(shop_id);
            senderImage = getIntent().getStringExtra(getString(R.string.intent_shop_image));

            senderName = getIntent().getStringExtra(getString(R.string.intent_shop_name));

            tvSenderName.setText(senderName);
            if (!senderImage.equals("null")) {
                imvSenderImage.setImageBitmap(imageConverter.convertFromBase64ToBitmap(senderImage));
            }

        }

        customerId_shopId = customer_id+"_"+shop_id;

        //my shop recycler
        chatMessagesList = new ArrayList<>();


        chatMessageAdapter = new ChatMessageAdapter(getApplication(), chatMessagesList);
        recyclerView = (RecyclerView)findViewById(R.id.reyclerview_message_list);


        RecyclerView.LayoutManager chatListLayoutManager = new LinearLayoutManager(getApplication(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(chatListLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(chatMessageAdapter);

        Log.i("customerId_shopId",customerId_shopId);
        databaseReference = FirebaseDatabase.getInstance().getReference().child("chats").child("message_list");
        final Query query = databaseReference.orderByChild("customerId_shopId").equalTo(customerId_shopId);

        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.i("check_child_change",dataSnapshot.getValue()+",");
                message = dataSnapshot.getValue(Messages.class);
                ChatMessages chatMessages = new ChatMessages(is_owner, message.getMessage(), senderName, message.getUpdated_at(), String.valueOf(message.getMy_id()), senderImage, String.valueOf(message.getMy_type()));
                chatMessagesList.add(chatMessages);
                chatMessageAdapter.notifyDataSetChanged();
                recyclerView.scrollToPosition(chatMessagesList.size()-1);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                message = dataSnapshot.getValue(Messages.class);
                Log.i("check_child_change1",dataSnapshot.getValue()+",");
                ChatMessages chatMessages = new ChatMessages(is_owner, message.getMessage(), senderName, message.getUpdated_at(), String.valueOf(message.getMy_id()), senderImage, String.valueOf(message.getMy_type()));
                chatMessagesList.add(chatMessages);
                chatMessageAdapter.notifyDataSetChanged();
                recyclerView.scrollToPosition(chatMessagesList.size()-1);

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

//        query.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                if (trackEventChangeListenerTime == 0){
//
//                    for (DataSnapshot getDataSnapsot: dataSnapshot.getChildren()) {
//
//                        message = getDataSnapsot.getValue(Messages.class);
//                        ChatMessages chatMessages = new ChatMessages(is_owner, message.getMessage(), senderName, message.getUpdated_at(), String.valueOf(message.getMy_id()), senderImage, String.valueOf(message.getMy_type()));
//                        chatMessagesList.add(chatMessages);
//                    }
//                    chatMessageAdapter.notifyDataSetChanged();
//                    Log.i("check_change_in_parents",databaseReference+",");
//
//
//                }
//                else {
//
//                    Log.i("check_next_q",databaseReference+",");
//
//                    final Query query1 = databaseReference.orderByChild("customerId_shopId").equalTo(customerId_shopId);
//
//                    query1.addChildEventListener(new ChildEventListener() {
//                        @Override
//                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                            Log.i("check_child_change",dataSnapshot.getValue()+",");
//
//                        }
//
//                        @Override
//                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//                            message = dataSnapshot.getValue(Messages.class);
//                            Log.i("check_child_change1",dataSnapshot.getValue()+",");
//                            ChatMessages chatMessages = new ChatMessages(is_owner, message.getMessage(), senderName, message.getUpdated_at(), String.valueOf(message.getMy_id()), senderImage, String.valueOf(message.getMy_type()));
//                            chatMessagesList.add(chatMessages);
//                            chatMessageAdapter.notifyDataSetChanged();
//                            Log.i("checkall_data",chatMessagesList.size()+"");
//
//                        }
//
//                        @Override
//                        public void onChildRemoved(DataSnapshot dataSnapshot) {
//
//                        }
//
//                        @Override
//                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//
//                        }
//
//                        @Override
//                        public void onCancelled(DatabaseError databaseError) {
//
//                        }
//                    });
//
//                }
//
//
//                trackEventChangeListenerTime++;
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Read the input field and push a new instance
                // of ChatMessage to the Firebase database
                Map<String, Messages> messagesHashMap = new HashMap<>();
                String timeMili = getRandomString();

                Messages messages = new Messages(Long.parseLong(timeMili),edtMessage.getText().toString(),my_id,my_type,partner_id,partner_type,customerId_shopId);

                databaseReference.child(String.valueOf(System.currentTimeMillis())).setValue(messages);
                //updat chat list
                ChatListFirebase chatListFirebase = new ChatListFirebase(is_owner,Integer.parseInt(shop_id),Integer.parseInt(customer_id),customerId_shopId,edtMessage.getText().toString(),System.currentTimeMillis());
                FirebaseDatabase.getInstance().getReference().child("chats").child("chat_list").child(customerId_shopId).setValue(chatListFirebase);


                edtMessage.setText("");
                btnSend.setEnabled(false);

            }
        });

    }

    private void initVolleyService() {

        resultCallback= new RequestResult() {
            @Override
            public void notifySuccess(int callBackCode, JSONObject response) {

                JSONObject jsonObject;
                if (callBackCode == getUserByIDCallbackCode) {

                    try {
                        jsonObject = response.getJSONObject("data");
                        senderName = jsonObject.getString("first_name");
                        senderImage = jsonObject.getString("image");

                        tvSenderName.setText(jsonObject.getString("first_name")+" "+
                                                jsonObject.getString("last_name"));
                        if (!jsonObject.getString("image").equals("null")) {
                            Log.i("track_image", jsonObject.getString("image"));
//
                            imvSenderImage.setImageBitmap(imageConverter.convertFromBase64ToBitmap(jsonObject.getString("image")));
                        }

//                        swipeRefreshLayout.setRefreshing(false);
//                        swipeRefreshLayout.setEnabled(false);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else {

                    Log.i("get_user",response+"");
                }
            }

            @Override
            public void notifyError(int callBackCode, VolleyError error) {
                Log.i("get_user_error",error.getMessage());

            }
        };
    }

    public String getRandomString() {

        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat sdf = new SimpleDateFormat("EE,d MMMM yyyy h:mma", Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+7"));
        String formattedDate = sdf.format(date);
        Log.i("get_mili","mili"+System.currentTimeMillis()+","+formattedDate);
         return String.valueOf(System.currentTimeMillis());

    }

    public String converMiliSecondToDate() {

        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE,MMMM d,yyyy h:mm,a", Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+7"));
        String formattedDate = sdf.format(date);
        Log.i("get_mili","mili"+System.currentTimeMillis()+","+formattedDate);
        return String.valueOf(System.currentTimeMillis());

    }


}
