package com.example.themaninthemiddl.souvenirshop.activities;

import android.Manifest;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.themaninthemiddl.souvenirshop.MainActivity;
import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;
import com.example.themaninthemiddl.souvenirshop.data.LocationService;
import com.example.themaninthemiddl.souvenirshop.data.StoreTemData;
import com.example.themaninthemiddl.souvenirshop.fragments.HomeFragment;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.florescu.android.rangeseekbar.RangeSeekBar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchActivity extends AppCompatActivity implements View.OnClickListener, MaterialSpinner.OnItemSelectedListener {

    private RangeSeekBar skbPrice;
    private TextView tvSkbMin, tvSkbMax, tvSkbDistanceMin, tvSkbDistanceMax;
    private SeekBar skbDistance;
    private EditText keyword;
    private AppCompatButton btnPopular, btnSearch;

    private GoogleMap google_map;
    private static final int REQUEST_CHECK_SETTINGS = 0x1;
    private static GoogleApiClient mGoogleApiClient;
    private static final int ACCESS_FINE_LOCATION_INTENT_ID = 3;
    private LatLng currentLocation = null;
    private float minPrice = 0, maxPrice = 0, maxDistance = 0, minDistance = 0;
    private String category = "0";
    boolean is_popular = false, is_nearby = false;
    private MaterialSpinner snCategory, snLocation;
    private List<String> categoryNameList = new ArrayList<String>();
    private List<String> categoryIdList = new ArrayList<String>();
    private List<String> locationList = new ArrayList<String>();

    //data in share preference
    private Authentication authentication;
    //data in share preference


    //server
    Map<String,String> param = new HashMap<String, String>();

    //server
    RequestResult resultCallback = null;
    VolleyService volleyService;
    private int getMinMaxPriceCallbackCode = 1;
    private String getMinMaxPriceUrl = "/product/min-max-price";
    private int getCategoryListCallbackCode = 2;

    private String getMinMaxDistantUrl = "/shop/min-max-distance";
    private int getMinMaxDistantCallbackCode = 3;

    private String getCategoryListUrl = "/category/list";
    private String selectedCategoryId = "0";
    private LinearLayout layoutNearby;
    private String user_id;
    private LocationService locationService;
    private StoreTemData storeSearchCondition;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(getResources().getString(R.string.search));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        skbPrice = (RangeSeekBar)findViewById(R.id.skb_price);
        skbDistance = (SeekBar) findViewById(R.id.skb_distance);

        tvSkbMin = (TextView)findViewById(R.id.tv_sb_min);
        tvSkbMax = (TextView)findViewById(R.id.tv_sb_max);
        tvSkbDistanceMax = (TextView)findViewById(R.id.tv_sb_distance_max);
        tvSkbDistanceMin = (TextView)findViewById(R.id.tv_sb_distance_min);
        btnPopular = (AppCompatButton)findViewById(R.id.btn_popular);
        btnSearch = (AppCompatButton)findViewById(R.id.btn_search);
        snCategory = (MaterialSpinner)findViewById(R.id.sn_category);
        layoutNearby = (LinearLayout)findViewById(R.id.layout_location);
        keyword = (EditText)findViewById(R.id.keyword);


        snLocation = (MaterialSpinner)findViewById(R.id.sn_location);
        locationList.add(getString(R.string.normal));
        locationList.add(getString(R.string.near_by));
        snLocation.setItems(locationList);

        snCategory.setOnItemSelectedListener(this);
        snLocation.setOnItemSelectedListener(this);

        btnSearch.setOnClickListener(this);
        btnPopular.setOnClickListener(this);
        storeSearchCondition = new StoreTemData(getApplication(),getString(R.string.search_condition));



//        skbPrice.setRangeValues(0,100);

        skbPrice.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Object minValue, Object maxValue) {

                minPrice = Float.valueOf(minValue.toString());
                maxPrice = Float.valueOf(maxValue.toString());

                Log.i("track_value",minValue.toString()+","+maxValue);
            }
        });

        skbDistance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                Log.i("track_value1",i+"");
                maxDistance = i;
                tvSkbDistanceMin.setText(String.valueOf(i)+"km");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        initVolleyCallback();
        volleyService = new VolleyService(resultCallback, getApplication());
        locationService = new LocationService(this);

        categoryNameList.add(getString(R.string.choose_a_category));
        categoryIdList.add("0");
        volleyService.makeApiRequest(getCategoryListCallbackCode, getCategoryListUrl,param);
        volleyService.makeApiRequest(getMinMaxPriceCallbackCode, getMinMaxPriceUrl,param);


        authentication = new Authentication(this);
        if (authentication.checkIfLogedIn()){
            user_id = authentication.getUserPreference().getString("id", null);
        }
        initGoogleAPIClient();
        setCurrentLocation();
        checkPermissions();
    }

    public void setCurrentLocation(){

        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            Log.i("location_enable_check","");
            mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    Log.i("location_enable_check",location.getLatitude()+"");

                    currentLocation = new LatLng(location.getLatitude(),location.getLongitude());

                    param.put("current_lat",String.valueOf(currentLocation.latitude));
                    param.put("current_lng",String.valueOf(currentLocation.longitude));

                    volleyService.makeApiRequest(getMinMaxDistantCallbackCode,getMinMaxDistantUrl,param);
                }
            });
        }
    }

    private void initVolleyCallback() {

        resultCallback = new RequestResult() {
            @Override
            public void notifySuccess(int callBackCode, JSONObject response) {

                JSONArray jsonArray = null;
                JSONObject jsonObject;

                if (callBackCode == getCategoryListCallbackCode) {

                    try {
                        jsonArray = response.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            jsonObject = jsonArray.getJSONObject(i);
                            Log.i("get_category", "" + jsonObject);
                            categoryNameList.add(jsonObject.getString("name"));
                            categoryIdList.add(jsonObject.getString("id"));


                        }
                        snCategory.setItems(categoryNameList);

                        Log.i("category_name", "" + categoryNameList);
                        Log.i("category_id", "" + categoryIdList);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else  if(callBackCode == getMinMaxPriceCallbackCode){

                    try {
                        jsonArray = response.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            jsonObject = jsonArray.getJSONObject(i);

                            minPrice = Float.valueOf(jsonObject.getString("min_price"));
                            maxPrice = Float.valueOf(jsonObject.getString("max_price"));


                            skbPrice.setRangeValues(Math.round(Float.valueOf(jsonObject.getString("min_price"))),
                                                    Math.round(Float.valueOf(jsonObject.getString("max_price")))+1);

                            tvSkbMin.setText(String.valueOf(Math.round(Float.valueOf(jsonObject.getString("min_price")))));
                            tvSkbMax.setText(String.valueOf(Math.round(Float.valueOf(jsonObject.getString("max_price")))+1));

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                else  if(callBackCode == getMinMaxDistantCallbackCode){

                    try {
                        jsonArray = response.getJSONArray("data");

                        Log.i("check_dis",jsonArray+"");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            jsonObject = jsonArray.getJSONObject(i);
                            Number maxDis =0, minDis = 0;
                            try {
                                 minDis = NumberFormat.getInstance().parse(jsonObject.getString("min_distance"));
                                 maxDis = NumberFormat.getInstance().parse((jsonObject.getString("max_distance")));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            Long minDistance = minDis.longValue();
                            Long maxDistance = maxDis.longValue();

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                skbDistance.setMin(Math.round(minDistance));
                            }

                            skbDistance.setMax(Math.round(maxDistance)+1);

                            tvSkbDistanceMin.setText(String.valueOf(Math.round(minDistance))+"km");
                            tvSkbDistanceMax.setText(String.valueOf(Math.round(maxDistance))+1+"km");

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void notifyError(int callBackCode, VolleyError error) {

                Log.i("get_error",""+ error);
            }
        };

    }

    @Override
    public void onClick(View view) {

        param.put("user_id",user_id);
        param.put("category_id",selectedCategoryId);
        param.put("minPrice",String.valueOf(minPrice));
        param.put("maxPrice",String.valueOf(maxPrice));
        param.put("minDistance",String.valueOf(minDistance));
        param.put("maxDistance",String.valueOf(maxDistance));
        param.put("is_nearby",String.valueOf(is_nearby));



        if (currentLocation != null) {
            param.put("current_lat", String.valueOf(currentLocation.latitude));
            param.put("current_lng", String.valueOf(currentLocation.longitude));
        }

        switch (view.getId()){

            case R.id.btn_popular:
                is_popular = true;
                break;

            case R.id.btn_search:
                is_popular = false;

                break;
        }

        storeSearchCondition.getEditor().putString("user_id",user_id);
        storeSearchCondition.getEditor().putString("category_id",selectedCategoryId);
        storeSearchCondition.getEditor().putString("minPrice",String.valueOf(minPrice));
        storeSearchCondition.getEditor().putString("maxPrice",String.valueOf(maxPrice));
        storeSearchCondition.getEditor().putString("minDistance",String.valueOf(minDistance));
        storeSearchCondition.getEditor().putString("maxDistance",String.valueOf(maxDistance));
        storeSearchCondition.getEditor().putBoolean("is_nearby",is_nearby);
        storeSearchCondition.getEditor().putString("current_lat", String.valueOf(currentLocation.latitude));
        storeSearchCondition.getEditor().putString("current_lng", String.valueOf(currentLocation.longitude));
        storeSearchCondition.getEditor().putBoolean("is_popular",is_popular);
        storeSearchCondition.getEditor().putBoolean("is_search", true);
        storeSearchCondition.getEditor().putString("keyword", keyword.getText().toString());



        storeSearchCondition.commitDataChange();



        Log.i("check_search_data",storeSearchCondition.getPreference().getString("category_id", "0")+""+param);


        onBackPressed();
        finish();


    }


    /* Check Location Permission for Marshmallow Devices */
    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(SearchActivity.this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED)
                requestLocationPermission();
            else
                showSettingDialog();
        } else
            showSettingDialog();

    }


    /* Initiate Google API Client  */
    private void initGoogleAPIClient() {
        //Without Google API Client Auto Location Dialog will not work
        mGoogleApiClient = new GoogleApiClient.Builder(getApplication())
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    /* Check Location Permission for Marshmallow Devices */

    /* Show Location Access Dialog */
    private void showSettingDialog() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);//Setting priotity of Location request to high
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);//5 sec Time interval for location update
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient to show dialog always when GPS is off

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        Log.i("location_enable", "locations is enable");
                        setCurrentLocation();

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(SearchActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    /*  Show Popup to access User Permission  */
    private void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(SearchActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(SearchActivity.this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);

        } else {
            ActivityCompat.requestPermissions(SearchActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case RESULT_OK:
                        Log.e("location_change", "Result OK");
                        setCurrentLocation();

                        //startLocationUpdates();
                        break;
                    case RESULT_CANCELED:
                        Log.e("location", "Result Cancel");
                        break;
                }
                break;
        }
    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

        switch (view.getId()){
            case R.id.sn_category:
                selectedCategoryId = categoryIdList.get(position);
                Log.i("categoryId",categoryIdList.get(position));
                break;

            case R.id.sn_location:
                if (position == 0) {
                    is_nearby = false;
                    layoutNearby.setVisibility(View.GONE);
                }
                else {
                    layoutNearby.setVisibility(View.VISIBLE);
                    is_nearby = true;
                }

                Log.i("check_location",position+"");
                break;
        }
    }

}
