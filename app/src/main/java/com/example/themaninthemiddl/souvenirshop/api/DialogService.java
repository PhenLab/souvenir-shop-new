package com.example.themaninthemiddl.souvenirshop.api;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.example.themaninthemiddl.souvenirshop.R;

import java.util.List;

/**
 * Created by lotphen on 12/29/17.
 */

public class DialogService {


    RequestDialog requestDialog = null;
    Context mContext;

    public DialogService(RequestDialog requestDialog, Context context){

        this.requestDialog = requestDialog;
        this.mContext = context;

    }


    public void makeYesNoDialog(final int callbackCode, final String title, final int message){

        new AlertDialog.Builder(mContext, R.style.CustomDialogTheme)

                .setMessage(mContext.getString(message))
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                        requestDialog.showDialog(callbackCode, -1);

                    }})
                .setNegativeButton(android.R.string.no, null).show();


    }

    public void makeChoosePhotoOptionDialog(final int callbackCode){

        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext,R.style.AppCompatAlertDialogStyle);

        builder.setTitle("Please choose on option");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override

            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo"))
                {
                    requestDialog.clickOnCameraOption(callbackCode);
                }

                else if (options[item].equals("Choose from Gallery"))
                {
                    requestDialog.clickOnGalleryOption(callbackCode);

                }

                else if (options[item].equals("Cancel")) {

                    dialog.dismiss();

                }

            }

        });

        builder.show();

    }

    public void makeChooseContactOptionDialog(final int callbackCode, List<String> contactList){

        contactList.add(mContext.getString(R.string.cancel));

        final String[] optionList = new String[ contactList.size() ];
        contactList.toArray( optionList );

        Log.i("check_contact:",optionList+"");

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext,R.style.AppCompatAlertDialogStyle);

        builder.setTitle("Please choose on option");

        builder.setItems(optionList, new DialogInterface.OnClickListener() {

            @Override

            public void onClick(DialogInterface dialog, int item) {

              requestDialog.showDialog(callbackCode, item);

            }

        });

        builder.show();

    }


}
