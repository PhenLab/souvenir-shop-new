package com.example.themaninthemiddl.souvenirshop.fragments;


import android.Manifest;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.activities.ShopDetailActivity;
import com.example.themaninthemiddl.souvenirshop.adapters.RecyclerViewItemClickListener;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;
import com.example.themaninthemiddl.souvenirshop.data.ImageConverter;
import com.example.themaninthemiddl.souvenirshop.data.Products;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.themaninthemiddl.souvenirshop.adapters.productAdapter;
import com.example.themaninthemiddl.souvenirshop.data.Shops;
import com.example.themaninthemiddl.souvenirshop.data.StoreTemData;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.GoogleMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
@RequiresApi(api = Build.VERSION_CODES.M)
public class HomeFragment extends Fragment implements RecyclerViewItemClickListener, SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView recyclerView;
    private productAdapter productAdapter;
    private List<Products> productList;

    private ImageView imgSearchFilter;

    //data in share preference
    private Authentication authentication;
    //data in share preference

    private GoogleMap google_map;
    private static final int REQUEST_CHECK_SETTINGS = 0x1;
    private static GoogleApiClient mGoogleApiClient;
    private static final int ACCESS_FINE_LOCATION_INTENT_ID = 3;
    //server
    Map<String,String> param = new HashMap<String, String>();

    //server
    RequestResult resultCallback = null;
    VolleyService volleyService;
    private int getProductListCallbackCode = 1;
    private String getProductListUrl = "/get-product-list";

    private ImageView addProduct, searchFilter;
    private ImageConverter imageConverter;
    private boolean is_search = false;
    private StoreTemData searchConditionData;
    private RelativeLayout layoutNoData;
    String next_query_id = null;

    SwipeRefreshLayout swipeRefreshLayout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_home, container, false);

        layoutNoData = (RelativeLayout) view.findViewById(R.id.layout_no_data);


        recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view);
        productList = new ArrayList<>();

        productAdapter = new productAdapter(getActivity(), productList,true);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(productAdapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    //Call your method here for next set of data
                    next_query_id = productList.get(productList.size()-1).getProduct_id();
                    Log.i("next_query_id",next_query_id);
                    param.put("next_query_id", next_query_id);

                    Log.i("load_product","scroll");

                    loadProducts();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }
        });


        authentication = new Authentication(getActivity());


        swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(this);

        initVolleyCallback();
        imageConverter = new ImageConverter(getActivity());
        volleyService = new VolleyService(resultCallback,getActivity());

        searchConditionData = new StoreTemData(getActivity(), getString(R.string.search_condition));


        if (authentication.checkIfLogedIn() == true){

            param.put("user_id",authentication.getUserPreference().getString("id",null));
            getProductListUrl = "/get-other-product-list";

        }
        loadProducts();
        Log.i("load_product","create");

        return view;
    }

    private void initVolleyCallback() {
        resultCallback = new RequestResult() {
            @Override
            public void notifySuccess(int callBackCode, JSONObject response) {

                Log.i("product_list_search",""+response.length()+", "+response);

                JSONArray jsonArray = null;
                JSONObject jsonObject;

                if (callBackCode == getProductListCallbackCode) {

                    if(next_query_id == null) {
                        productList.clear();
                        Log.i("clear_product_list","n "+next_query_id);

                    }
                    Log.i("product_list_before",""+productList.size());

                    boolean isError = false;
                    try {
                        jsonArray = response.getJSONArray("data");
                        Log.i("product_list_search_s",""+jsonArray.length());

                        for (int i = 0; i < jsonArray.length(); i++) {
                            jsonObject = jsonArray.getJSONObject(i);
                            Products product = new Products(false, jsonObject.getString("id"),jsonObject.getString("image"),
                                    jsonObject.getString("name"), jsonObject.getString("price"),
                                    jsonObject.getString("category_name"), jsonObject.getString("shop_name"));
                            productList.add(product);
                        }
                        Log.i("product_list_after",""+productList.size());

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("rupp", "Error converting documents");
                        isError = true;
                    }
                    if (productList.size() > 0){
                        recyclerView.setVisibility(View.VISIBLE);
                        layoutNoData.setVisibility(View.GONE);

                    }
                    else {

                        recyclerView.setVisibility(View.GONE);
                        layoutNoData.setVisibility(View.VISIBLE);
                    }
                    swipeRefreshLayout.setRefreshing(false);
                    productAdapter.notifyDataSetChanged();


                }

                Log.i("product_data",""+response);
            }

            @Override
            public void notifyError(int callBackCode, VolleyError error) {
                Log.i("product_data error",""+error.getMessage());
                swipeRefreshLayout.setRefreshing(false);
            }
        };
    }

    @Override
    public void onRecyclerViewItemClick(int position) {

        Toast.makeText(getActivity(),"this is id"+position,Toast.LENGTH_LONG).show();

    }

    @Override
    public void onRefresh() {

        loadProducts();
        Log.i("load_product","refresh");

    }

    @Override
    public void onResume() {
        super.onResume();
        productList.clear();

        is_search = searchConditionData.getPreference().getBoolean("is_search", false);

        param.put("is_search",String.valueOf(is_search));
        if (is_search){

            param.put("keyword",searchConditionData.getPreference().getString("keyword",null));
            param.put("user_id",searchConditionData.getPreference().getString("user_id",null));
            param.put("category_id",searchConditionData.getPreference().getString("category_id",null));
            param.put("minPrice",searchConditionData.getPreference().getString("minPrice",null));
            param.put("maxPrice",searchConditionData.getPreference().getString("maxPrice",null));
            param.put("minDistance",searchConditionData.getPreference().getString("minDistance",null));
            param.put("maxDistance",searchConditionData.getPreference().getString("maxDistance",null));
            param.put("current_lat", searchConditionData.getPreference().getString("current_lat",null));
            param.put("current_lng", searchConditionData.getPreference().getString("current_lng",null));
            param.put("is_popular", String.valueOf(searchConditionData.getPreference().getBoolean("is_popular",false)));
            param.put("is_nearby", String.valueOf(searchConditionData.getPreference().getBoolean("is_nearby",false)));

            param.put("next_query_id",null);
            is_search = false;

        }else {
            param.clear();

            getProductListUrl = "/get-product-list";
            if (authentication.checkIfLogedIn() == true){

                param.put("user_id",authentication.getUserPreference().getString("id",null));
                getProductListUrl = "/get-other-product-list";

            }
        }
        Log.i("is_search_resume",productList.size()+", "+is_search+""+param);
        Log.i("load_product","resume");

        loadProducts();


    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        searchConditionData.clearData();

    }

    private void loadProducts() {

        swipeRefreshLayout.setRefreshing(true);

        volleyService.makeApiRequest(getProductListCallbackCode,getProductListUrl,param);

    }

    /* Check Location Permission for Marshmallow Devices */
    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED)
                requestLocationPermission();
            else
                showSettingDialog();
        } else
            showSettingDialog();

    }


    /* Initiate Google API Client  */
    private void initGoogleAPIClient() {
        //Without Google API Client Auto Location Dialog will not work
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    /* Check Location Permission for Marshmallow Devices */

    /* Show Location Access Dialog */
    private void showSettingDialog() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);//Setting priotity of Location request to high
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);//5 sec Time interval for location update
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient to show dialog always when GPS is off

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.

                        Log.i("check_loc", "locations is enable");

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    /*  Show Popup to access User Permission  */
    private void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);

        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case RESULT_OK:
                        Log.e("Settings", "location enable");

                        //startLocationUpdates();
                        break;
                    case RESULT_CANCELED:
                        Log.e("location_change", "Result Cancel");
                        break;
                }
                break;
        }
    }


}
