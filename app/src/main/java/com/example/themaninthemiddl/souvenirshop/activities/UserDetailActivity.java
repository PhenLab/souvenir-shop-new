package com.example.themaninthemiddl.souvenirshop.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.themaninthemiddl.souvenirshop.MainActivity;
import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.api.DialogService;
import com.example.themaninthemiddl.souvenirshop.api.RequestDialog;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;
import com.example.themaninthemiddl.souvenirshop.data.ImageConverter;
import com.example.themaninthemiddl.souvenirshop.data.StoreTemData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class UserDetailActivity extends AppCompatActivity implements View.OnClickListener{

    private Toolbar toolbar;
    private ImageView editProfile;
    private RelativeLayout signOut, rtlChangePassword;
    private CardView profilePhicture;
    private static final int GET_IMAGE_FROM_CAMERA_CODE = 1;
    private static final int GET_IMAGE_FROM_GALLERY_CODE = 2;

    private Uri userImgUri;

    private Bitmap userImgBitmap;
    private ImageConverter imageConverter;
    private String imageBase64String;
    private TextView firstName, lastName, userName, phone, email;
    private ImageView image;
    private Button saveProfileImage;
    private RelativeLayout saveLayout;
    private boolean is_select_save = false;

    //data in share preference
    private String userData = "user_data";
    //data in share preference

    private Authentication authentication;
    //server
    Map<String,String> param = new HashMap<String, String>();

    //server
    RequestResult resultCallback = null;
    VolleyService volleyService;
    private int getUserByIDCallbackCode = 1;
    private int updateUserCallbackCode = 2;

    private String getUserByIDUrl = "/get-user-by-id";
    private String updateUserUrl = "/update-user-image";
    SwipeRefreshLayout swipeRefreshLayout;
    private RequestDialog  requestDialog;
    private DialogService dialogService;
    private int choosePhotoCallbackCode = 1;
    private int getYesNoDialogCallCode = 2;
    private StoreTemData storeWhenUseAccount;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail);

        toolbar= (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(getResources().getString(R.string.user_detail));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        storeWhenUseAccount = new StoreTemData(this,getString(R.string.store_when_use_account));
        editProfile = (ImageView)findViewById(R.id.edit_user);
        signOut = (RelativeLayout) findViewById(R.id.sign_out);

        saveLayout = (RelativeLayout)findViewById(R.id.save_layout);
        rtlChangePassword = (RelativeLayout)findViewById(R.id.rtl_change_password);

        rtlChangePassword.setOnClickListener(this);
        editProfile.setOnClickListener(this);
        signOut.setOnClickListener(this);

        authentication = new Authentication(this);

        firstName = (TextView)findViewById(R.id.first_name);
        lastName = (TextView)findViewById(R.id.last_name);
        userName = (TextView)findViewById(R.id.user_name);
        phone = (TextView)findViewById(R.id.phone);
        email = (TextView)findViewById(R.id.email);
        image = (ImageView) findViewById(R.id.image);
        saveProfileImage = (Button) findViewById(R.id.save_profile_image);

        saveProfileImage.setOnClickListener(this);
        image.setOnClickListener(this);

        imageConverter = new ImageConverter(this);

        initVolleyCallback();
        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));

        volleyService = new VolleyService(resultCallback,this);
        param.put("user_id",authentication.getUserPreference().getString("id",null));

        loadUserData();
        //choose photo

        initDialogCallback();
        dialogService = new DialogService(requestDialog,this);
    }

    private void loadUserData() {
        swipeRefreshLayout.setRefreshing(true);
        volleyService.makeApiRequest(getUserByIDCallbackCode,getUserByIDUrl,param);

    }


    private void initDialogCallback() {

        requestDialog = new RequestDialog() {
            @Override
            public void showDialog(int dialogCallbackCode, int item) {

                if (dialogCallbackCode == getYesNoDialogCallCode) {
                    storeWhenUseAccount.getEditor().putBoolean(getString(R.string.is_after_use_account),  true);

                    storeWhenUseAccount.commitDataChange();
                    authentication.logOut();
                    onBackPressed();
                    finish();
                }

            }

            @Override
            public void clickOnCameraOption(int dialogCallbackCode) {

                if (dialogCallbackCode == choosePhotoCallbackCode)
                    launchCameraClick();

            }

            @Override
            public void clickOnGalleryOption(int dialogCallbackCode) {
                if (dialogCallbackCode == choosePhotoCallbackCode)
                    openGalleryClick();
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadUserData();
    }

    private void initVolleyCallback() {
        resultCallback = new RequestResult() {
            @Override
            public void notifySuccess(int callBackCode, JSONObject response) {

                JSONObject jsonObject;
                if (callBackCode == getUserByIDCallbackCode) {

                    try {
                        jsonObject = response.getJSONObject("data");
                        phone.setText(jsonObject.getString("phone"));
                        email.setText(jsonObject.getString("email"));
                        firstName.setText(jsonObject.getString("first_name"));
                        lastName.setText(jsonObject.getString("last_name"));
                        userName.setText(jsonObject.getString("user_name"));
                        if (!(jsonObject.getString("image")).equals("null")) {
                            Log.i("track_image", jsonObject.getString("image"));
                            image.setImageBitmap(imageConverter.convertFromBase64ToBitmap(jsonObject.getString("image")));
                        }
//
                        swipeRefreshLayout.setRefreshing(false);
                        swipeRefreshLayout.setEnabled(false);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else {

                    saveLayout.setVisibility(View.GONE);
                    Log.i("get_user",response+"");
                }
            }

            @Override
            public void notifyError(int callBackCode, VolleyError error) {

                swipeRefreshLayout.setRefreshing(false);
                swipeRefreshLayout.setEnabled(false);
                Toast.makeText(UserDetailActivity.this, "update user error" + error, Toast.LENGTH_SHORT).show();


            }
        };
    }

    @Override
    public void onClick(View view) {
//        Toast.makeText(this, "this is save profile", Toast.LENGTH_SHORT).show();

        switch (view.getId()){

            case R.id.rtl_change_password:

                startActivity(new Intent(getApplication(),ChangePasswordActivity.class));
                break;

            case R.id.edit_user:

                startActivity(new Intent(getApplication(),EditUserActivity.class));
                break;

            case R.id.image:

                dialogService.makeChoosePhotoOptionDialog(choosePhotoCallbackCode);

                break;

            case R.id.sign_out:
                dialogService.makeYesNoDialog(getYesNoDialogCallCode,null,R.string.confirm_sign_out);

                break;

            case R.id.save_profile_image:

                param.put("image",imageBase64String);
                param.put("id",authentication.getUserPreference().getString("id",null));

                volleyService.makeApiRequest(updateUserCallbackCode,updateUserUrl,param);
//                Toast.makeText(this, "this is save profile "+authentication.getUserPreference().getString("id",null), Toast.LENGTH_SHORT).show();
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GET_IMAGE_FROM_GALLERY_CODE && resultCode == RESULT_OK){

            userImgUri = data.getData();


            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), userImgUri);
                userImgBitmap = bitmap;
                userImgBitmap = imageConverter.loadFitImage(userImgUri,300,300);
                image.setImageBitmap(userImgBitmap);

                //convert from bitmap to base64 string
                imageBase64String = imageConverter.convertFromBitmapToBase64(userImgBitmap);
                //decode base64 string to image

                saveLayout.setVisibility(View.VISIBLE);

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        else if (requestCode == GET_IMAGE_FROM_CAMERA_CODE && resultCode == RESULT_OK){

            Bitmap bitmap = (Bitmap) data.getExtras().get("data");

            userImgBitmap = imageConverter.loadFitImage(imageConverter.convertFromBitmapToUri(bitmap),500,500);
            image.setImageBitmap(userImgBitmap);

            //convert from bitmap to base64 string
            imageBase64String = imageConverter.convertFromBitmapToBase64(userImgBitmap);
            saveLayout.setVisibility(View.VISIBLE);

        }
    }


    //camera
    public void launchCameraClick(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent,GET_IMAGE_FROM_CAMERA_CODE);

    }

    //gallery
    public void openGalleryClick(){

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent,GET_IMAGE_FROM_GALLERY_CODE);

    }

}
