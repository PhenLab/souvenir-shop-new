package com.example.themaninthemiddl.souvenirshop.data;

/**
 * Created by The Man In The Middl on 5/28/2018.
 */

public class ChatListFirebase {

    private int shop_id;
    private int customer_id;
    private String customerId_shopId;
    private String last_messages;
    private Long updated_at;
    private boolean is_shop_message;

    public ChatListFirebase(){

    }
    public ChatListFirebase(boolean is_shop_message, int shop_id, int customer_id, String customerId_shopId, String last_messages, Long updated_at) {
        this.shop_id = shop_id;
        this.customer_id = customer_id;
        this.customerId_shopId = customerId_shopId;
        this.last_messages = last_messages;
        this.updated_at = updated_at;
        this.is_shop_message = is_shop_message;
    }

    public boolean isIs_shop_message() {
        return is_shop_message;
    }

    public void setIs_shop_message(boolean is_shop_message) {
        this.is_shop_message = is_shop_message;
    }

    public int getShop_id() {
        return shop_id;
    }

    public void setShop_id(int shop_id) {
        this.shop_id = shop_id;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public String getCustomerId_shopId() {
        return customerId_shopId;
    }

    public void setCustomerId_shopId(String customerId_shopId) {
        this.customerId_shopId = customerId_shopId;
    }

    public String getLast_messages() {
        return last_messages;
    }

    public void setLast_messages(String last_messages) {
        this.last_messages = last_messages;
    }

    public Long getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Long updated_at) {
        this.updated_at = updated_at;
    }
}
