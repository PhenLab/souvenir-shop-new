package com.example.themaninthemiddl.souvenirshop.activities;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.android.volley.VolleyError;
import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.adapters.ChatListAdapter;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.AllChatList;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;
import com.example.themaninthemiddl.souvenirshop.data.ChatListFirebase;
import com.example.themaninthemiddl.souvenirshop.data.FirbaseServices;
import com.example.themaninthemiddl.souvenirshop.data.TimeHelper;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChatListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    Toolbar toolbar;
    List<AllChatList> allChatLists;
    ChatListAdapter chatListAdapter;
    List<ChatListFirebase> chatListFirebaseList;
    String shopId = null;

    private Authentication authentication;
    //server
    Map<String,String> param = new HashMap<String, String>();

    //server
    RequestResult resultCallback = null;
    VolleyService volleyService;
    private int getUserByIDCallbackCode = 1;
    private String getUserByIDUrl = "/get-user-by-id";
    int storChatlistSize =0;
    ChatListFirebase chatListFirebase;
    int changeDatabaseTime= 0;
    private  boolean is_shop_owner = true;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_list);

        toolbar= (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(getResources().getString(R.string.shop_chat_list));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);


        //my shop recycler
        allChatLists = new ArrayList<>();


        chatListAdapter = new ChatListAdapter(getApplication(), allChatLists);
        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.recycler_view_chat_list);


        RecyclerView.LayoutManager chatListLayoutManager = new LinearLayoutManager(getApplication(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(chatListLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(chatListAdapter);
        //my shop recycler
        chatListFirebaseList = new ArrayList<>();
        shopId = getIntent().getStringExtra(getString(R.string.shop_id));
        initVolleyService();
        volleyService = new VolleyService(resultCallback, this);

        getChatListData(Integer.parseInt(shopId));

        Log.i("check_chatList",chatListFirebaseList+"");

    }

    private void initVolleyService() {
        resultCallback = new RequestResult() {
            @Override
            public void notifySuccess(int callBackCode, JSONObject response) {
                JSONObject jsonObject;
                Log.i("check_request","time");
                    try {
                        jsonObject = response.getJSONObject("data");

                        AllChatList allChatList = new AllChatList(is_shop_owner,chatListFirebaseList.get(storChatlistSize).isIs_shop_message(),
                                chatListFirebaseList.get(storChatlistSize).getLast_messages(),
                                jsonObject.getString("first_name")+jsonObject.getString("last_name"),
                                TimeHelper.converMiliSecondToDate(chatListFirebaseList.get(storChatlistSize).getUpdated_at()),
                                shopId,jsonObject.getString("image"),jsonObject.getString("id"));
                        allChatLists.add(allChatList);
                        storChatlistSize--;
                        Log.i("check_all_chat_li",allChatLists.size()+"");

                        if (storChatlistSize >=0 ){

                            param.put("user_id",String.valueOf(chatListFirebaseList.get(storChatlistSize).getCustomer_id()));
                            Log.i("check_request",""+storChatlistSize+"post"+param);

                            volleyService.makeApiRequest(getUserByIDCallbackCode,getUserByIDUrl,param);

                        }else {
                            Log.i("check_request_stop",""+allChatLists);

                            swipeRefreshLayout.setRefreshing(false);
                            chatListAdapter.notifyDataSetChanged();
                        }
                        chatListAdapter.notifyDataSetChanged();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

            }

            @Override
            public void notifyError(int callBackCode, VolleyError error) {
                swipeRefreshLayout.setRefreshing(false);
                Log.i("request_error",error.getMessage());
            }
        };
    }

    public void getChatListData(int shop_id){


//        FirbaseServices.getShopChatList(shop_id).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//
//                allChatLists.clear();
//                chatListFirebaseList.clear();
//
//                Log.i("checkChangetime",  "test");
//
//                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
//                    Log.i("checkValueInChange", postSnapshot + "");
//                    chatListFirebase = postSnapshot.getValue(ChatListFirebase.class);
//                    chatListFirebaseList.add(chatListFirebase);
//
//                }
//                if (chatListFirebaseList.size() > 0) {
//                    storChatlistSize = chatListFirebaseList.size() - 1;
//                    param.put("user_id", String.valueOf(chatListFirebaseList.get(storChatlistSize).getCustomer_id()));
//                    Log.i("check_request_outA", chatListFirebaseList.size() + "," + storChatlistSize);
//                    volleyService.makeApiRequest(getUserByIDCallbackCode, getUserByIDUrl, param);
//
//                }
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });

        FirbaseServices.getShopChatList(shop_id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                allChatLists.clear();
                chatListFirebaseList.clear();

                Log.i("checkChangetime",  "test");

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Log.i("checkValueInChange", postSnapshot + "");
                    chatListFirebase = postSnapshot.getValue(ChatListFirebase.class);
                    chatListFirebaseList.add(chatListFirebase);

                }
                if (chatListFirebaseList.size() > 0) {
                    storChatlistSize = chatListFirebaseList.size() - 1;
                    param.put("user_id", String.valueOf(chatListFirebaseList.get(storChatlistSize).getCustomer_id()));
                    Log.i("check_request_outA", chatListFirebaseList.size() + "," + storChatlistSize);
                    volleyService.makeApiRequest(getUserByIDCallbackCode, getUserByIDUrl, param);

                }

            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        Log.i("check_time","test");
        chatListAdapter.notifyDataSetChanged();

    }

    @Override
    public void onRefresh() {
        getChatListData(Integer.parseInt(shopId));
    }
}
