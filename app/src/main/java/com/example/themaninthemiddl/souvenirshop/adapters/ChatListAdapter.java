package com.example.themaninthemiddl.souvenirshop.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.activities.ChatActivity;
import com.example.themaninthemiddl.souvenirshop.activities.ShopDetailActivity;
import com.example.themaninthemiddl.souvenirshop.data.AllChatList;
import com.example.themaninthemiddl.souvenirshop.data.ChatListFirebase;
import com.example.themaninthemiddl.souvenirshop.data.ImageConverter;
import com.example.themaninthemiddl.souvenirshop.data.Shops;
import com.example.themaninthemiddl.souvenirshop.data.TimeHelper;

import java.util.List;

/**
 * Created by The Man In The Middl on 4/29/2018.
 */

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.MyViewHolder> {

    private Context mContext;
    private List<AllChatList> allChatLists;
    private ImageConverter imageConverter;
    private String base64String;
    boolean isCustomise = false;
    int testStep = 0;

    private RecyclerViewItemClickListener recyclerViewItemClickListener;
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Log.i("in_onCreateViewHolder",testStep+"");
        testStep++;
        imageConverter = new ImageConverter(mContext);

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_chat_list_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Log.i("in_onBindViewHolder",testStep+"");
        testStep++;
        AllChatList allChatList = allChatLists.get(position);
        holder.tvPartnerName.setText(allChatList.getPartnerName());
        holder.tvMessaageTime.setText(allChatList.getLastMessageTime());

        if (allChatList.isIs_shop_owner()){
            if (allChatList.isIs_shop_message())
                holder.tvLastMessage.setText("You: "+allChatList.getLastMessage());

            else
                holder.tvLastMessage.setText(allChatList.getLastMessage());
        }
        else {
            if (! allChatList.isIs_shop_message())
                holder.tvLastMessage.setText("You: " + allChatList.getLastMessage());
            else if (!allChatList.isIs_shop_message())
                holder.tvLastMessage.setText(allChatList.getLastMessage());
        }


        if (!allChatList.getPartnerImage().equals("null")) {
            holder.imvPartnerImage.setImageBitmap(imageConverter.convertFromBase64ToBitmap(allChatList.getPartnerImage()));
        }
    }

    @Override
    public int getItemCount() {
        return allChatLists.size();
    }



    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView  tvPartnerName, tvLastMessage, tvMessaageTime;
        public ImageView imvPartnerImage;

        public MyViewHolder(View view) {
            super(view);
            Log.i("in_MyViewHolder",testStep+"");
            testStep++;
            imvPartnerImage = (ImageView) view.findViewById(R.id.imv_partner_image);
            tvLastMessage = (TextView) view.findViewById(R.id.tv_last_message);
            tvMessaageTime = (TextView)view.findViewById(R.id.tv_message_time);
            tvPartnerName = (TextView)view.findViewById(R.id.tv_partner_name);

            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {


            AllChatList allChatList = allChatLists.get(getAdapterPosition());
            Intent intent = new Intent(mContext,ChatActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(mContext.getString(R.string.is_owner), allChatList.isIs_shop_owner());

            if (allChatList.isIs_shop_owner() == true) {

                intent.putExtra(mContext.getString(R.string.customer_image), allChatLists.get(getAdapterPosition()).getPartnerImage());
                intent.putExtra(mContext.getString(R.string.customer_name), allChatLists.get(getAdapterPosition()).getPartnerName());

                intent.putExtra(mContext.getString(R.string.shop_id), allChatLists.get(getAdapterPosition()).getShopId());
                intent.putExtra(mContext.getString(R.string.customer_id), allChatLists.get(getAdapterPosition()).getCustomerId());
            }
            else {

                intent.putExtra(mContext.getString(R.string.intent_shop_image), allChatLists.get(getAdapterPosition()).getPartnerImage());
                intent.putExtra(mContext.getString(R.string.intent_shop_name), allChatLists.get(getAdapterPosition()).getPartnerName());

                intent.putExtra(mContext.getString(R.string.shop_id), allChatLists.get(getAdapterPosition()).getShopId());
                intent.putExtra(mContext.getString(R.string.customer_id), allChatLists.get(getAdapterPosition()).getCustomerId());

            }
            mContext.startActivity(intent);

        }
    }

    public ChatListAdapter(Context mContext, List<AllChatList> allChatLists) {

        this.mContext = mContext;
        this.allChatLists = allChatLists;

    }

}
